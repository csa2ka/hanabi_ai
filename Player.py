from Card import Card
from colored_print import color_card_print
from Info import Info


class Player:
    game = None

    def __init__(self, game_, player_index):
        self.hand = []
        self.player_index = player_index
        Player.game = game_

    def start_game(self):
        self.draw(5)
        # self.classify_hand()

    def make_a_move(self):
        # Should check whether it plays, discards, or informate
        # if there is no token it can't inforamte, if there is too much token can't discard
        best_info = self.best_info()
        print(best_info[0], best_info[1], best_info[2])
        self.classify_a_hand(self.hand)
        a_playable_index = -1
        for i in range(len(self.hand)):
            if self.hand[i].playable_for_sure:
                a_playable_index = i
        if a_playable_index > -1:           # If there is a playable they play it
            self.play_a_card(a_playable_index)
        elif best_info[2] > 1:                # If there is a good info informate
            self.informate(best_info[0], best_info[1])
        else:                               # Discard
            discard_index = -1
            for i in range(len(self.hand)): # the discradebles
                if self.hand[i].discardable_for_sure:
                    discard_index = i
            if discard_index > -1:
                self.discard_a_card(discard_index)
            else:
                done = False
                for c in self.hand:          # or at least something which is not needed for sure
                    if not c.playable_for_sure and not c.endangered_for_sure:
                        discard_index = self.hand.index(c)
                        done = True
                if done:                    # If everything is needed informate
                    self.discard_a_card(discard_index)
                else:
                    self.informate(best_info[0], best_info[1])


    def best_info(self):
        # tells, what is the best info which can be given, regatding all the players
        best_player = None
        best_info_usefulness = 0
        best_info = None
        for player in self.game.players:
            if player.player_index != self.player_index:

                for card_i in player.hand:                                 # All the cards
                    # TODO: all colors, and all numbers should be checked, not just the colors, and numbers of the cards
                    # TODO: in hand
                    indexes = []
                    for color_n_number_i in range(2):

                        candidate_info = None
                        for card_j in player.hand:

                            if color_n_number_i == 0:
                                if card_i.number == card_j.number:          # Number infos
                                    indexes.append(player.hand.index(card_j))
                                candidate_info = Info("number", card_i.number, indexes)
                            if color_n_number_i == 1:
                                if card_i.color == card_j.color:  # Color infos
                                    indexes.append(player.hand.index(card_j))
                                candidate_info = Info("color", card_i.color, indexes)
                        self.informate_shadow(player, candidate_info)
                        candidate_info_usefulness = self.classify_a_hand(player.hand, shadow_classification=True)
                        if candidate_info_usefulness >= best_info_usefulness:
                            best_info_usefulness = candidate_info_usefulness
                            best_info = candidate_info
                            best_player = player
        return [best_player, best_info, best_info_usefulness]

    def classify_card(self, c, shadow_classification=False):  # TODO: this shadow suff is kinda bad
        # Updates a card based on this players view on the cards, and the info arrays
        color = c.what_is_it_shadow()[0]
        number = c.what_is_it_shadow()[1]

        # print(" Classification: color, number", color, number, shadow_classification)
        # color = c.color
        # number = c.number

        #c.print_info()
        usefulness = 0

        playable_cards = self.playables()
        endangered_cards = self.endangered_ones()
        discardable_cards = self.discardables()

        possibly_endangered = False
        possibly_playable = False
        playable_for_sure = False
        endangered_for_sure = False
        discardable_for_sure = False

        usf_pe = 0.25
        usf_pp = 0.25
        usf_pfs = 1
        usf_efs = 0.9
        usf_dfs = 0.5

        # Has info like an endangered card
        for endangered_card in endangered_cards:
            if endangered_card.number == number:
                possibly_endangered = True
                # if c.possibly_endangered:
                #    usefulness += usf_pe

        # Has info like a playable card
        for playable_card in playable_cards:
            if playable_card.number == number:
                possibly_playable = True
                # if not c.possibly_playable:
                #    usefulness += usf_pp

        # It is sure to be playable
        for playable_card in playable_cards:
            if playable_card.color == color and playable_card.number == number:
                playable_for_sure = True
                # usefulness += usf_pfs

        # It is sure to be endangered
        for endangered_card in endangered_cards:
            if endangered_card.color == color and endangered_card.number == number:
                endangered_for_sure = True
                # usefulness += usf_efs

        # It is sure to be discardable
        for discard_card in discardable_cards:
            if discard_card.color == color and discard_card.number == number:
                discardable_for_sure = True
                # usefulness += usf_dfs

        if c.possibly_endangered != possibly_endangered:
            usefulness += usf_pe
        if c.possibly_playable != possibly_playable:
            usefulness += usf_pp
        if c.playable_for_sure != playable_for_sure:
            usefulness += usf_pfs
        if c.endangered_for_sure != endangered_for_sure:
            usefulness += usf_efs
        if c.discardable_for_sure != discardable_for_sure:
            usefulness += usf_dfs





        if shadow_classification:
            c.possibly_endangered_shadow = possibly_endangered
            c.possibly_playable_shadow = possibly_playable
            c.playable_for_sure_shadow = playable_for_sure
            c.endangered_for_sure_shadow = endangered_for_sure
            c.discardable_for_sure_shadow = discardable_for_sure
        else:
            c.possibly_endangered = possibly_endangered
            c.possibly_playable = possibly_playable
            c.playable_for_sure = playable_for_sure
            c.endangered_for_sure = endangered_for_sure
            c.discardable_for_sure = discardable_for_sure
        # print("card usefulness: ", usefulness)
        return usefulness

    def classify_a_hand(self, hand, shadow_classification=False):
        usefulness = 0
        for c in hand:
            usefulness += self.classify_card(c, shadow_classification)
        return usefulness

    def playables(self):
        # Returns all the cards which will be the next ones to play on each pile
        playable_cards = []
        for i in range(self.game.COLOR_NUMBER):
            playable_cards.append(Card(i + 1, len(self.game.color_piles[i]) + 1))
        return playable_cards

    def discardables(self):
        # Returns all the card which is already in the colorpiles
        discardable_cards = []
        for color_i in range(self.game.COLOR_NUMBER):
            for number_i in range(len(self.game.color_piles[color_i])):
                discardable_cards.append(Card(color_i + 1, number_i + 1))
        return discardable_cards

    def endangered_ones(self):
        endangered_cards = []
        for color_i in range(self.game.COLOR_NUMBER):
            for number_i in range(5):
                discarded_count = 0
                initial_count = 1
                for c in self.game.discard_pile.cards:
                    if c.color == color_i + 1 and c.number == number_i + 1:
                        discarded_count += 1
                # for p in self.game.players:      # TODO: count cards in hands, it's tricky: depends on the viewpoint
                #    if p.player_index != self.player_index:
                #        for c in p.hand:
                #            if c.color == color_i + 1 and c.number == number_i + 1:
                #                count += 1
                if number_i + 1 < 5:
                    initial_count += 1
                if number_i + 1 == 1:
                    initial_count += 1
                if initial_count - discarded_count == 1:
                    endangered_cards.append(Card(color_i + 1, number_i + 1))
                # print("Card:", color_i, number_i, " init_count: ", initial_count, "discarded_c: ", discarded_count,
                # len(endangered_cards))
                # if len(endangered_cards)>0: print(endangered_cards[0].color, endangered_cards[0].number)
        return endangered_cards

    def play_a_card(self, index):

        print("PPPLPPLLLLLLAAAAAAAAYYYYYYYY")
        self.game.attempt_to_play_a_card(self.hand.pop(index))
        self.draw()

    def discard_a_card(self, index):
        print("DDDDDDDIIIIISSSSSCCCCAAARRRDDDDDD")
        Player.game.discard_pile.add_card(self.hand.pop(index))
        self.draw()

    @staticmethod
    def informate(player, info):
        print("IIIINNNNFFFOORRRMMAATTEEEEE")
        player.info_receiving(info)

    @staticmethod
    def informate_shadow(player, info):
        player.info_receiving_shadow(info)

    def info_receiving(self, info):  # color_or_number, value, indexes, turn
        # Updates the color.info() and number_info() arrays values according the info
        for hand_i in range(self.game.HAND_SIZE):  # 0 1 2 3 4
            for info_i in range(5):
                if info.color_or_number == "color":
                    self.hand[hand_i].color_info[info_i][0] = -1
                    self.hand[hand_i].color_info[info_i][1] = self.game.turn
                elif info.color_or_number == "number":
                    self.hand[hand_i].number_info[info_i][0] = -1
                    self.hand[hand_i].number_info[info_i][1] = self.game.turn
                else:
                    "Hát ez meg mi a szösz? A color_or_number elromlott."
            for indi in info.indexes:
                if info.color_or_number == "color":
                    self.hand[indi].color_info[info.value - 1][0] = 1
                elif info.color_or_number == "number":
                    self.hand[indi].number_info[info.value - 1][0] = 1
                else:
                    "Hát ez meg mi a szösz? A color_or_number elromlott."

    def info_receiving_shadow(self, info):  # color_or_number, value, indexes, turn
        # Updates the color.info() and number_info() arrays values according the info
        # info.print_info()
        #print("Receivier: ", self.player_index)
        for card in self.hand:
            card.shadow_clone()
        """self.game.HAND_SIZE"""
        for hand_i in range(len(self.hand)):  # 0 1 2 3 4
            for info_i in range(5):
                if info.color_or_number == "color":
                    self.hand[hand_i].color_info_shadow[info_i][0] = -1
                    self.hand[hand_i].color_info_shadow[info_i][1] = self.game.turn
                elif info.color_or_number == "number":
                    self.hand[hand_i].number_info_shadow[info_i][0] = -1
                    self.hand[hand_i].number_info_shadow[info_i][1] = self.game.turn
                else:
                    "Hát ez meg mi a szösz? A color_or_number elromlott."
            for indi in info.indexes:
                if info.color_or_number == "color":
                    self.hand[indi].color_info_shadow[info.value - 1][0] = 1
                elif info.color_or_number == "number":
                    self.hand[indi].number_info_shadow[info.value - 1][0] = 1
                else:
                    "Hát ez meg mi a szösz? A color_or_number elromlott."
            # for i in range(5):
            #    print(self.hand[i].number_info_shadow[0])

    def draw(self, amount=1):
        for i in range(amount):
            if len(Player.game.deck.cards) > 0:                   # TODO: watch out for hand size problems
                self.hand.append(Player.game.deck.draw())

    def print_hand(self):
        print("P", self.player_index, end=": ")
        for c in self.hand:
            color_card_print(c.color, c.number)
            # print(c.color, c.number)
            if c.playable_for_sure:
                print("PLAYABLE!!!")
            if c.endangered_for_sure:
                print("ENDANGERED!!!")
        print()
