import random
from colored_print import color_card_print
from Card import Card


class Deck:
    def __init__(self, fill="not fill", shuffle="not shuffle"):
        self.cards = []
        if fill == "fill":
            self.fill_up()
        if shuffle == "shuffle":
            self.shuffle()
        # self.print_cards()

    def add_card(self, card):
        self.cards.append(card)

    def draw(self):
        return self.cards.pop(-1)

    def shuffle(self):
        length = len(self.cards)
        for k in range(10):
            for i in range(length):
                j = random.randint(-1, length - 1)
                card = self.cards[i]
                self.cards[i] = self.cards[j]
                self.cards[j] = card

    def print_cards(self):
        # for i in range(len(self.cards)):
        #    print(i, ": ", self.cards[i].color, self.cards[i].number)
        for i in range(len(self.cards)):
            color_card_print(self.cards[i].color, self.cards[i].number)
        print(" ")

    def fill_up(self):
        self.clear()
        for j in range(5):
            for k in range(5):
                if k == 0:
                    self.add_card(Card(j + 1, k + 1))
                if k <= 3:
                    self.add_card(Card(j + 1, k + 1))
                self.add_card(Card(j + 1, k + 1))

    def clear(self):
        self.cards.clear()
