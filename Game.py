from Deck import Deck
from Player import Player
from colored_print import color_card_print


class Game:
    MAX_LIFE = 50
    MAX_INFO_TOKEN = 8
    HAND_SIZE = 5
    PLAYER_NUMBER = 2
    COLOR_NUMBER = 5

    def __init__(self):
        self.color_piles = []
        for i in range(Game.COLOR_NUMBER):
            color_pile = []
            self.color_piles.append(color_pile)

        self.deck = Deck("fill", "shuffle")
        self.discard_pile = Deck()

        self.players = []
        for i in range(Game.PLAYER_NUMBER):
            self.players.append(Player(self, i))

        self.turn = 0
        self.life = Game.MAX_LIFE
        self.info_token = Game.MAX_INFO_TOKEN

        self.last_card_drown = False
        self.last_round_counter = 0

    def a_game(self):
        self.start_game()
        while not self.game_ended():
            for p in self.players:
                p.make_a_move()
                self.print_game_state()
                self.turn += 1
        self.game_over()

    def a_game_ng(self):
        self.start_game()
        while not self.game_ended():
            for p in self.players:
                p.make_a_move()
                # self.print_game_state()
                self.turn += 1
        # self.game_over()

    def game_ended(self):
        if self.life <= 0:
            return True
        if len(self.deck.cards) == 0:
            self.last_card_drown = True
        if self.last_card_drown:
            self.last_round_counter += 1
        if self.last_round_counter == self.PLAYER_NUMBER:
            return True
        return False

    def start_game(self):
        self.last_card_drown = False
        self.last_round_counter = 0
        for p in self.players:
            p.start_game()
            self.print_game_state()
            best_info = p.best_info()
            print(best_info[0], best_info[1], best_info[2])
            if best_info[0] is not None:
                print("Player: ", best_info[0].player_index)
            if best_info[1] is not None:
                best_info[1].print_info()
            print("Usefulness:", best_info[2])

    def a_player_turn(self):
        # find player
        # player do something
        self.turn += 1

    def attempt_to_play_a_card(self, card):
        placed = False
        for i in range(5):  # 0 1 2 3 4
            if i == card.color - 1:
                if len(self.color_piles[i]) == card.number - 1:
                    self.color_piles[i].append(card)
                    placed = True
        if not placed:
            self.discard_pile.add_card(card)
            self.lose_life()

    def lose_life(self):
        # if self.life <= 1:
        #    self.game_over()
        # else:
        self.life -= 1

    def game_over(self):
        print("---------------    GAME OVER    ----------------------")
        print("Pontszám: ", self.points())

    def points(self):
        points = 0
        for pile in self.color_piles:
            points += len(pile)
        return points

    def print_game_state(self):
        print("===============    ", self.turn, ". kör    ======================")
        print("Hűzópakli: ", end="")
        self.deck.print_cards()
        print()
        self.players_print_cards()
        print()
        self.print_piles()
        print()
        print("Eldobott lapok: ", end="")
        self.discard_pile.print_cards()

    def print_piles(self):
        for number_i in range(5):
            for pile in self.color_piles:
                if len(pile) > number_i:
                    color_card_print(pile[number_i].color, pile[number_i].number)
                    print("", end=" ")
                else:
                    print(".", end=" ")
            print("")

    def players_print_cards(self):
        for p in self.players:
            p.print_hand()
