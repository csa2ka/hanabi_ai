def color_card_print(color, number):
    # could depend on the terminal colors
    # prints a number with a colored background
    if color == 1:
        print('\x1b[1;30;42m' + str(number) + '\x1b[0m', end="")  # green
    if color == 2:
        print('\x1b[1;30;43m' + str(number) + '\x1b[0m', end="")  # yellow
    if color == 3:
        print('\x1b[1;30;44m' + str(number) + '\x1b[0m', end="")  # blue
    if color == 4:
        print('\x1b[1;37;40m' + str(number) + '\x1b[0m', end="")  # white
    if color == 5:
        print('\x1b[1;30;41m' + str(number) + '\x1b[0m', end="")  # red
    if color == 6:
        print('\x1b[1;37;45m' + str(number) + '\x1b[0m', end="")  # pink/purple
    if color == 7:
        print('\x1b[1;30;47m' + str(number) + '\x1b[0m', end="")  # gray
