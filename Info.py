class Info:
    def __init__(self, color_or_number, value, indexes):
        self.color_or_number = color_or_number
        self.value = value
        self.indexes = indexes

        # For example: Info("color", 2, [0,1,5], 12)
        # Which means: The 0. 1. 5. card in your hand has color 2 this was told you at turn 12

    def print_info(self):
        print()
        print("---------- Info: ", self.color_or_number, self.value, self.indexes)
