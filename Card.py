class Card:
    def __init__(self, color, number):
        self.color = color
        self.number = number
        # self.---_info stores what information do the player who holds this card have about it's color, number and also
        # when was those information come
        self.color_info = []
        # filling up self.info with no info
        for i in range(5):
            self.color_info.append([0, -1])
        # color info, number info, turn when color info came, turn when number info came
        # -1 means it's not this; 1 means it is this;
        # -1 means no info come yet; for example 14 means this info come at turn 14
        self.number_info = []
        for i in range(5):
            self.number_info.append([0, -1])

        self.number_info_shadow = []
        for i in range(5):
            self.number_info_shadow.append([0, -1])

        self.color_info_shadow = []
        for i in range(5):
            self.color_info_shadow.append([0, -1])

        # Classification for the hand
        self.possibly_endangered = False
        self.possibly_playable = False
        self.playable_for_sure = False
        self.endangered_for_sure = False
        self.discardable_for_sure = False

        self.possibly_endangered_shadow = False
        self.possibly_playable_shadow = False
        self.playable_for_sure_shadow = False
        self.endangered_for_sure_shadow = False
        self.discardable_for_sure_shadow = False

        # Classification for others:
        self.endangered_index = 4  # 3-0 means there are this many copies in the game still
        self.playability_index = 5  # 5-1 means it is this far for you to be able to play it

    def what_is_it(self):
        color = -1
        number = -1
        for i in range(5):
            if self.color_info[i] == 1:
                color = i + 1
            if self.number_info[i] == 1:
                number = i + 1
        return [color, number]

    def what_is_it_shadow(self):
        color = -1
        number = -1
        for i in range(5):

            if self.color_info_shadow[i][0] == 1:
                # print('color_here!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                color = i + 1

            if self.number_info_shadow[i][0] == 1:
                # print('number_here!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                number = i + 1
        # print("HEHEHEHEHEHEHEH:", number, color)
        return [color, number]

    def set_classification(self, classifications):
        self.playable_for_sure = False
        self.endangered_for_sure = False
        self.discardable_for_sure = False
        self.possibly_playable = False
        self.possibly_endangered = False
        if classifications[0] == 1:
            self.playable_for_sure = True
        if classifications[1] == 1:
            self.endangered_for_sure = True
        if classifications[2] == 1:
            self.discardable_for_sure = True
        if classifications[3] == 1:
            self.possibly_playable = True
        if classifications[4] == 1:
            self.possibly_endangered = True

    def set_classification_shadow(self, classifications):
        self.playable_for_sure_shadow = False
        self.endangered_for_sure_shadow = False
        self.discardable_for_sure_shadow = False
        self.possibly_playable_shadow = False
        self.possibly_endangered_shadow = False
        if classifications[0] == 1:
            self.playable_for_sure_shadow = True
        if classifications[1] == 1:
            self.endangered_for_sure_shadow = True
        if classifications[2] == 1:
            self.discardable_for_sure_shadow = True
        if classifications[3] == 1:
            self.possibly_playable_shadow = True
        if classifications[4] == 1:
            self.possibly_endangered_shadow = True

    def print_info(self):
        print("COLOR:", end=" ")
        for i in range(5):
            print(self.color_info_shadow[i], end = " ")
        print()
        print("NUMBER:", end=" ")
        for i in range(5):
            print(self.number_info_shadow[i], end = " ")
        print()

    def shadow_clone(self):
        for i in range(len(self.color_info)):
            self.color_info_shadow[i][0] = self.color_info[i][0]
            self.color_info_shadow[i][1] = self.color_info[i][1]
        for i in range(len(self.number_info)):
            self.number_info_shadow[i][0] = self.number_info[i][0]
            self.number_info_shadow[i][1] = self.number_info[i][1]
